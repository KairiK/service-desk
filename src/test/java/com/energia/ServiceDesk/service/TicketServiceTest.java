package com.energia.ServiceDesk.service;

import com.energia.ServiceDesk.model.Ticket;
import org.junit.After;
import org.junit.Test;
import org.springframework.jdbc.core.JdbcTemplate;

import java.time.LocalDate;

import static org.junit.Assert.assertEquals;


public class TicketServiceTest {
    private TicketService ticketService = new TicketService();
    private JdbcTemplate jdbcTemplate;

    @After
    public void tearDown() {
        jdbcTemplate.execute("DELETE FROM tickets");
    }

    @Test
    public void addsTicketToDatabase() {
        LocalDate date = LocalDate.parse("2019-07-15");
        Ticket ticket = new Ticket("Test ticket", "Test description", "email", 5);
        ticketService.addTicket(ticket, jdbcTemplate);

        String description = jdbcTemplate.queryForObject("SELECT description FROM tickets WHERE title='Test ticket';", String.class);
        assertEquals("Test description", description);
        String email = jdbcTemplate.queryForObject("SELECT email FROM tickets WHERE title='Test ticket';", String.class);
        assertEquals("email", email);
        int priority = jdbcTemplate.queryForObject("SELECT priority FROM tickets WHERE title='Test ticket';", Integer.class);
        assertEquals(5, priority);
        String status = jdbcTemplate.queryForObject("SELECT status FROM tickets WHERE title='Test ticket';", String.class);
        assertEquals("pending", status);
    }

    @Test
    public void updatesTicket() {
        LocalDate date = LocalDate.parse("2019-07-15");
        Ticket ticket = new Ticket("Test ticket", "Test description", "email", 5);
        ticketService.addTicket(ticket, jdbcTemplate);

        Ticket updatedTicket = new Ticket(111, "Test ticket", "Test description update", "email update", 4, "closed", date);
        ticketService.updateTicket(updatedTicket, jdbcTemplate);

        String description = jdbcTemplate.queryForObject("SELECT description FROM tickets WHERE title='Test ticket';", String.class);
        assertEquals("Test description update", description);
        String email = jdbcTemplate.queryForObject("SELECT email FROM tickets WHERE title='Test ticket';", String.class);
        assertEquals("email update", email);
        int priority = jdbcTemplate.queryForObject("SELECT priority FROM tickets WHERE title='Test ticket';", Integer.class);
        assertEquals(4, priority);
        String status = jdbcTemplate.queryForObject("SELECT status FROM tickets WHERE title='Test ticket';", String.class);
        assertEquals("closed", status);
    }
}