package com.energia.ServiceDesk.service;

import com.energia.ServiceDesk.model.Ticket;
import org.junit.After;
import org.junit.Test;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

public class TicketViewServiceTest {
    private TicketViewService ticketViewService = new TicketViewService();
    private JdbcTemplate jdbcTemplate;

    @After
    public void tearDown() {
        jdbcTemplate.execute("DELETE FROM tickets");
    }

    @Test
    public void returnsOpenTickets() {
        jdbcTemplate.execute("INSERT INTO tickets (title, description, email, priority, status, date) " +
                "VALUES ('Test1', 'Test1 description', 'testemail1', 1, 'pending', '2019-07-15');");
        jdbcTemplate.execute("INSERT INTO tickets (title, description, email, priority, status, date) " +
                "VALUES ('Test2', 'Test2 description', 'testemail1', 1, 'open', '2019-07-15');");
        jdbcTemplate.execute("INSERT INTO tickets (title, description, email, priority, status, date) " +
                "VALUES ('Test3', 'Test3 description', 'testemail1', 1, 'closed', '2019-07-15');");

        ArrayList openTickets = ticketViewService.getOpenTickets(jdbcTemplate);
        assertEquals(2, openTickets.size());
    }

    @Test
    public void returnsOpenTickets_noOpenTicketsInDb() {
        jdbcTemplate.execute("INSERT INTO tickets (title, description, email, priority, status, date) " +
                "VALUES ('Test1', 'Test1 description', 'testemail1', 1, 'closed', '2019-07-15');");
        jdbcTemplate.execute("INSERT INTO tickets (title, description, email, priority, status, date) " +
                "VALUES ('Test2', 'Test2 description', 'testemail1', 1, 'closed', '2019-07-15');");
        jdbcTemplate.execute("INSERT INTO tickets (title, description, email, priority, status, date) " +
                "VALUES ('Test3', 'Test3 description', 'testemail1', 1, 'closed', '2019-07-15');");

        ArrayList openTickets = ticketViewService.getOpenTickets(jdbcTemplate);
        assertEquals(0, openTickets.size());
    }

    @Test
    public void retrievesCorrectTicket() {
        jdbcTemplate.execute("INSERT INTO tickets (title, description, email, priority, status, date) " +
                "VALUES ('Test', 'Test description', 'testemail', 1, 'open', '2019-07-15');");
        int id = jdbcTemplate.queryForObject("SELECT id FROM tickets WHERE title='Test';", Integer.class);

        Ticket ticket = ticketViewService.getRequiredTicketById(id, jdbcTemplate);
        assertEquals("Test description", ticket.getDescription());
        assertEquals("testemail", ticket.getEmail());
        assertEquals("open", ticket.getStatus());
        assertEquals(1, ticket.getPriority());
    }
}