package com.energia.ServiceDesk;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jdbc.core.JdbcTemplate;

@SpringBootApplication
public class ServiceDeskApplication implements CommandLineRunner {

	@Autowired
	private
	JdbcTemplate jdbcTemplate;

	public static void main(String[] args) {
		SpringApplication.run(ServiceDeskApplication.class, args);
	}

	@Override
	public void run(String... args) {
		jdbcTemplate.execute("CREATE TABLE IF NOT EXISTS tickets ( " +
				"id SERIAL," +
				"date DATE," +
				"title TEXT," +
				"description TEXT, " +
				"email TEXT, " +
				"priority SMALLINT, " +
				"status TEXT);");
	}
}
