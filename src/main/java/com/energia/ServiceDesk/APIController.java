package com.energia.ServiceDesk;

import com.energia.ServiceDesk.model.Ticket;
import com.energia.ServiceDesk.service.TicketService;
import com.energia.ServiceDesk.service.TicketViewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;

@CrossOrigin
@RestController
public class APIController {
    private TicketService ticketService = new TicketService();
    private TicketViewService ticketViewService = new TicketViewService();

    @Autowired
    private
    JdbcTemplate jdbcTemplate;

    public APIController() {
    }

    @GetMapping("/index")
    ArrayList returnAllOpenTickets(){
        return ticketViewService.getOpenTickets(jdbcTemplate);
    }

    @PostMapping("/addTicket")
    void addTicket(@RequestBody Ticket ticket) {
        ticketService.addTicket(ticket, jdbcTemplate);
    }

    @PostMapping("/getTicketData")
    Ticket returnChosenTicket(@RequestBody HashMap<String, String> ticketNumber){
        int ticketId = Integer.parseInt(ticketNumber.get("chosenTicketId"));
        return ticketViewService.getRequiredTicketById(ticketId, jdbcTemplate);
    }

    @PostMapping("/modifyTicket")
    void modifyTicket (@RequestBody Ticket ticket) {
        ticketService.updateTicket(ticket, jdbcTemplate);
    }
}

