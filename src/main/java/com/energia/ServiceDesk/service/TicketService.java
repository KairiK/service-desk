package com.energia.ServiceDesk.service;

import com.energia.ServiceDesk.model.Ticket;
import org.springframework.jdbc.core.JdbcTemplate;
import java.time.LocalDate;

public class TicketService {

    public void addTicket(Ticket ticket, JdbcTemplate jdbcTemplate){
        LocalDate date = LocalDate.now();
        String sqlCommand = "INSERT INTO tickets (title, description, email, priority, status, date) " +
                "VALUES ('"+ticket.getTitle()+"', '"+ticket.getDescription()+"', '"+ticket.getEmail()+"', '"+ticket.getPriority()+"', 'pending', '" + date +"');";
        jdbcTemplate.execute(sqlCommand);
    }

    public void updateTicket(Ticket ticket, JdbcTemplate jdbcTemplate){
        String sqlCommand = "UPDATE tickets SET title='"+ ticket.getTitle() +
                "', description='"+ ticket.getDescription() +
                "', priority='" + ticket.getPriority() +
                "', status='" + ticket.getStatus() +
                "', email='" + ticket.getEmail() +
                "' WHERE id="+ticket.getId()+
                ";";
        jdbcTemplate.execute(sqlCommand);
    }
}
