package com.energia.ServiceDesk.service;

import com.energia.ServiceDesk.model.Ticket;
import org.springframework.jdbc.core.JdbcTemplate;

import java.time.LocalDate;
import java.util.ArrayList;

public class TicketViewService {

    public ArrayList getOpenTickets(JdbcTemplate jdbcTemplate){
        String sqlCommand = "SELECT * FROM tickets WHERE status='open' OR status='pending';";
        return (ArrayList) jdbcTemplate.queryForList(sqlCommand);
    }

    public Ticket getRequiredTicketById(int ticketId, JdbcTemplate jdbcTemplate){
        ArrayList<Ticket> tickets = (ArrayList<Ticket>) jdbcTemplate.query("SELECT * FROM tickets WHERE id="+ticketId+";", (resultSet, rowNum) -> {
            String title = resultSet.getString("title");
            String email = resultSet.getString("email");
            int id = resultSet.getInt("id");
            String description = resultSet.getString("description");
            int priority = resultSet.getInt("priority");
            String status = resultSet.getString("status");
            LocalDate date = LocalDate.parse(resultSet.getString("date"));
            return new Ticket(id, title, description, email, priority, status, date);
        });
        return tickets.get(0);
    }
}
