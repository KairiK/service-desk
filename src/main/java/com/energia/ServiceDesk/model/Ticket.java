package com.energia.ServiceDesk.model;

import java.time.LocalDate;

public class Ticket {
    private int id;
    private String title;
    private String description;
    private String email;
    private int priority;
    private String status;
    private LocalDate date;

    public Ticket() {
    }

    public Ticket(String title, String description, String email, int priority) {
        this.title = title;
        this.email = email;
        this.description = description;
        this.priority = priority;
    }

    public Ticket(int id, String title, String description, String email, int priority, String status, LocalDate date) {
        this.title = title;
        this.id = id;
        this.email = email;
        this.description = description;
        this.priority = priority;
        this.status = status;
        this.date = date;
    }

    public String getTitle() {
        return title;
    }

    public String getEmail() {
        return email;
    }

    public String getDescription() {
        return description;
    }

    public int getPriority() {
        return priority;
    }

    public int getId() {
        return id;
    }

    public String getStatus() {
        return status;
    }
}