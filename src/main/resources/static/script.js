function sortTableByPriority() {
  var table, rows, switching, i, x, y, shouldSwitch;
  table = document.getElementById("ticketsTable");
  switching = true;
  while (switching) {
    switching = false;
    rows = table.rows;
    for (i = 1; i < (rows.length - 1); i++) {
      shouldSwitch = false;
      x = rows[i].getElementsByTagName("TD")[5];
      y = rows[i + 1].getElementsByTagName("TD")[5];
      if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
        shouldSwitch = true;
        break;
      }
    }
    if (shouldSwitch) {
      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
      switching = true;
    }
  }
}

function sortTableByStatus() {
  var table, rows, switching, i, x, y, shouldSwitch;
  table = document.getElementById("ticketsTable");
  switching = true;
  while (switching) {
    switching = false;
    rows = table.rows;
    for (i = 1; i < (rows.length - 1); i++) {
      shouldSwitch = false;
      x = rows[i].getElementsByTagName("TD")[6];
      y = rows[i + 1].getElementsByTagName("TD")[6];
      if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
        shouldSwitch = true;
        break;
      }
    }
    if (shouldSwitch) {
      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
      switching = true;
    }
  }
}

function sortTableByDate() {
  var table, rows, switching, i, x, y, shouldSwitch;
  table = document.getElementById("ticketsTable");
  switching = true;
  while (switching) {
    switching = false;
    rows = table.rows;
    for (i = 1; i < (rows.length - 1); i++) {
      shouldSwitch = false;
      x = rows[i].getElementsByTagName("TD")[1];
      y = rows[i + 1].getElementsByTagName("TD")[1];
      if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
        shouldSwitch = true;
        break;
      }
    }
    if (shouldSwitch) {
      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
      switching = true;
    }
  }
}

document.querySelector('#addTicket').onsubmit = async function(event){
	event.preventDefault()
	var title = document.querySelector("#title").value;
	var description = document.querySelector("#description").value;
    var priority = document.querySelector("#priority").value;
	var email = document.querySelector("#email").value;

    document.querySelector('#title').value = "";
    document.querySelector('#description').value = "";
    document.querySelector('#email').value = "";
    document.querySelector("#addTicketResponse").innerHTML = "Submission successful!";

	var APIurl = 'http://localhost:8080/addTicket';
	await fetch(APIurl, {
		method: "POST",
		body: JSON.stringify({title, description, email, priority}),
		headers: {
			'Accept': 'application/json',
			'Content-Type':'application/json'
		}
	})
}

function setSelectedIndex(s, i){
    s.options[i-1].selected = true;
    return;
}

async function loadChosenTicket(){
    var chosenTicketId = document.querySelector("#changeTicketValue").value;
    if(chosenTicketId!=="") {
    var currentTicketData = await fetch("/getTicketData", {
        method: "POST",
        body: JSON.stringify({chosenTicketId}),
        headers: {
        	'Accept': 'application/json',
        	'Content-Type':'application/json'
        }
    });
    var ticketData = await currentTicketData.json();
    document.querySelector("#changeTicketTitle").value = ticketData.title;
    document.querySelector("#changeTicketDescription").value = ticketData.description;
    document.querySelector("#changeTicketEmail").value = ticketData.email;
    var priorityId;
    if(ticketData.priority===1){
        priorityId = 5;
    } else if (ticketData.priority===2){
        priorityId = 4;
    } else if (ticketData.priority===4){
        priorityId = 2;
    } else if (ticketData.priority===5){
        priorityId = 1;
    } else {
        priorityId = 3;
    }
    setSelectedIndex(document.getElementById("changeTicketPriority"),priorityId);
    var statusId;
    if(ticketData.status==="open"){
        statusId = 1;
    } else if (ticketData.status==="closed"){
        statusId = 2;
    } else {
        statusId = 3;
    }
    setSelectedIndex(document.getElementById("changeTicketStatus"),statusId);
    } else {
    document.querySelector("#errorMessage").innerHTML = "Error! No ticket was chosen. Please try again."
    }
}

document.querySelector('#changeTicket').onsubmit = async function(event){
	event.preventDefault();
	var id = document.querySelector("#changeTicketValue").value;
	var title = document.querySelector("#changeTicketTitle").value;
	var description = document.querySelector("#changeTicketDescription").value;
    var priority = document.querySelector("#changeTicketPriority").value;
    var status = document.querySelector("#changeTicketStatus").value;
	var email = document.querySelector("#changeTicketEmail").value;

    document.querySelector("#changeTicketTitle").value = "";
    document.querySelector("#changeTicketDescription").value = "";
    document.querySelector("#changeTicketEmail").value = "";
    document.querySelector("#changeTicketResponse").innerHTML = "Change successful!";

	var APIurl = 'http://localhost:8080/modifyTicket';
	await fetch(APIurl, {
		method: "POST",
		body: JSON.stringify({id, title, description, email, priority, status}),
		headers: {
			'Accept': 'application/json',
			'Content-Type':'application/json'
		}
	})
}


async function retrieveInfoFromDatabaseAndPresentInTable(){
    var APIurl = 'http://localhost:8080/index';
    var data = await fetch(APIurl);
    var allTickets = await data.json();
    while(allTickets.length>0) {
        var ticket = allTickets.pop();
        document.querySelector('#ticketsTable').innerHTML +=
            "<tr><td>" + ticket.id + "</td><td>" + ticket.date + "</td><td>" + ticket.title + "</td><td>" + ticket.description + "</td><td>" + ticket.email + "</td><td>" + ticket.priority + "</td><td>" + ticket.status + "</td></tr>"
        document.querySelector('#changeTicketValue').innerHTML +=
                		     "<option value='" + ticket.id + "'>" + ticket.id + "</option>"
        }
    }

retrieveInfoFromDatabaseAndPresentInTable();