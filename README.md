SERVICE DESK

Project setup
- Install Java 8 and pgAdmin 4 for postgreSQL
- Open pgAdmin 4 and create account/log in with username postgres and password postgres.
Username and password can be changed, but the settings in application.properties should be changed
accordingly.
- Run ServiceDeskApplication to run the app.
-- ServiceDeskApplication will create the necessary table in the database and APIController will take care
of receiving requests from the client and forwarding queries.
-- TicketService and TicketViewService will make the queries to the postgreSQL database.